App.info({
  id: 'com.fmg.pratik',
  name: 'Find-me-a-game',
  author: 'Pratik Bhoir',
  email: 'pratik@bhoir.org',
  version: '1.3.0'
});

 App.icons({
   // Android
   'android_ldpi': 'resources/icons/icon-36x36.png',
   'android_mdpi': 'resources/icons/icon-48x48.png',
   'android_hdpi': 'resources/icons/icon-72x72.png',
   'android_xhdpi': 'resources/icons/icon-96x96.png',

   //iOS - uncomment following lines for iphone app
   //'iphone': 'resources/icons/icon-60.png',
   //'iphone_2x': 'resources/icons/icon-60@2x.png'
 });

 App.launchScreens({

   // Android
   'android_ldpi_portrait': 'resources/splash/splash-200x320.png',
   'android_ldpi_landscape': 'resources/splash/splash-320x200.png',
   'android_mdpi_portrait': 'resources/splash/splash-320x480.png',
   'android_mdpi_landscape': 'resources/splash/splash-480x320.png',
   'android_hdpi_portrait': 'resources/splash/splash-480x800.png',
   'android_hdpi_landscape': 'resources/splash/splash-800x480.png',
   'android_xhdpi_portrait': 'resources/splash/splash-720x1280.png',
   'android_xhdpi_landscape': 'resources/splash/splash-1280x720.png',

   // iOS - uncomment following lines for iphone app
   //'iphone': 'resources/splash/Default~iphone.png',
   //'iphone_2x': 'resources/splash/Default@2x~iphone.png',
   //'iphone5': 'resources/splash/Default-568h@2x~iphone.png'
 });

App.setPreference('StatusBarOverlaysWebView', 'false');
App.setPreference('StatusBarBackgroundColor', '#000000');

App.accessRule('*');
