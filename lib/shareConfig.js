if (Meteor.isClient) {
    // Configuration for share buttons
    // Documentation: https://github.com/meteorclub/shareit
  ShareIt.init({
    siteOrder: ['facebook', 'twitter', 'googleplus', 'pinterest'],
    sites: {
      'facebook': {
        'appId': '1671677869712206',
        'version': 'v2.5',
        'description': 'Check out this app - Find me a game'
      }
    },
    iconOnly: true,
    applyColors: true
  });
}
