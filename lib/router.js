Router.route('/', function () {
  this.render('mainLayout');
});

Router.route('/about', function () {
  this.render('aboutLayout');
});
