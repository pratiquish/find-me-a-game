QuestionsDb = new Meteor.Collection("questionsdb");

if (Meteor.isClient) {
  Meteor.subscribe("questionsdb");
  Session.setDefault("nextQId", "1");

  Template.questions.helpers({
    nextQuestion: function () {
      var nextQuestion = QuestionsDb.findOne({qId: Session.get("nextQId")});
			return nextQuestion;
    }
  });

  Template.questions.events({
    'click a': function (event) {
      // increment the counter when button is clicked
      if (event.currentTarget.id == "Yes"){
        Session.set('nextQId', QuestionsDb.findOne({qId: Session.get("nextQId")}).nextOneId);
      }else if (event.currentTarget.id == "No") {
        Session.set('nextQId', QuestionsDb.findOne({qId: Session.get("nextQId")}).nextTwoId);
      }
    }
  });

  Template.navbar.events({
    "click #reset": function(event, template){
       Session.set("nextQId", "1");
    }
  });
}

if (Meteor.isServer) {
  Meteor.startup(function () {
    // code to run on server at startup
    Meteor.publish("questionsdb", function () {
      return QuestionsDb.find({});
  });
  });
  Houston.add_collection(QuestionsDb);
  Houston.add_collection(Meteor.users);
}
