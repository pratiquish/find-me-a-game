Meteor.startup(function () {
  // Insert sample data if the collection is empty
  if (QuestionsDb.find().count() === 0) {
      var questions = JSON.parse(Assets.getText("docs.json")).allquestions;
      for(var i = 0; i < questions.length; i++) {
          QuestionsDb.insert(questions[i]);
      }
  }
});
